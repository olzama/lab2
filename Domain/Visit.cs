using System;

namespace Domain
{
    public class Visit
    {
        public int Id { get; set; }
        public string? Company { get; set; }
        public int GuestId { get; set; }
        public Guest? Guest { get; set; }
        public string TypeOfVisit { get; set; }
        public DateTime? Planned { get; set; }
    }
}