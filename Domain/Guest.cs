﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class Guest
    {
        public int Id { get; set; }
        public string FirstName { get; set; } = default!;
        public string LastName { get; set; } = default!;
        public string? Email { get; set; }
        public ICollection<Visit>? VisitorLogs { get; set; }
        public string Type { get; set; }
    }
}