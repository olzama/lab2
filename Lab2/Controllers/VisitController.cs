using System;
using Microsoft.AspNetCore.Mvc;
using Domain;
using GuestControlService;

namespace Lab2.Controllers

{ 
   [Route("api/visits")]
   [ApiController]
   public class VisitController : ControllerBase
    {
        private readonly IGuestControlService _service;
        public VisitController(IGuestControlService service)
        {
            _service = service;
        }
        
        [HttpGet("{id}")]
        public ActionResult<Visit> GetVisit(int id)
        {
            try
            {
                var visit = _service.Visits.Get(id);
                if (visit == null)
                {
                    return StatusCode(404, "Visit does not exist.");
                }
                return Ok(visit);
            }
            catch (Exception e)
            {
                return StatusCode(400, e.Message);
            }
        } 
        [HttpPost]
        public ActionResult<Visit> PostVisit(Visit visit)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var visitCreated = _service.Visits.Add(visit);
                    return Created($"api/visitor-logs/{visitCreated.Id}", visitCreated);
                    
                }
                catch (Exception e)
                {
                    return StatusCode(400, e.Message);
                }
            }
            return BadRequest(ModelState);
        }

    }
}
