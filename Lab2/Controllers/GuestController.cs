﻿using System;
using Microsoft.AspNetCore.Mvc;
using Domain;
using GuestControlService;

namespace Lab2.Controllers

{
   [Route("api/guests")]
   [ApiController]
   public class GuestController : ControllerBase
    {
        private readonly IGuestControlService _service;
        public GuestController(IGuestControlService service)
        {
            _service = service;
        }
        
        [HttpGet("{id}")]
        public ActionResult<Visit> GetGuest(int id)
        {
            try
            {
                var guest = _service.Guests.Get(id);
                if (guest == null)
                {
                    return StatusCode(404, "guest does not exist.");
                }
                return Ok(guest);
            }
            catch (Exception e)
            {
                return StatusCode(400, e.Message);
            }
        } 


        [HttpGet("{id}")]
        public ActionResult<Visit> GetVisit(int id)
        {
            try
            {
                var visit = _service.Visits.Get(id);
                if (visit == null)
                {
                    return StatusCode(404, "Visit does not exist.");
                }
                return Ok(visit);
            }
            catch (Exception e)
            {
                return StatusCode(400, e.Message);
            }
        }

    }
}
