﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace GuestControlService
{
    public class GuestControlService: IGuestControlService
    {
        private DbContext _context;
        public GuestControlService(DbContext context)
        {
            _context = context;
        }
        
        public IGuestService Guests =>
            GetService<GuestService>(() => new GuestService(_context));

        public IVisitService Visits =>
            GetService<IVisitService>(() => new VisitService(_context));

        private readonly Dictionary<Type, object> _repoCache = new Dictionary<Type, object>();

        // Factory method
        public TService GetService<TService>(Func<TService> serviceCreationMethod)
        {
            if (_repoCache.TryGetValue(typeof(TService), out var repo))
            {
                return (TService)repo;
            }

            repo = serviceCreationMethod()!;
            _repoCache.Add(typeof(TService), repo);
            return (TService)repo;
        }
    }
    
    public interface IGuestControlService
    {
        IGuestService Guests { get; } 
        IVisitService Visits { get; }
    }
}