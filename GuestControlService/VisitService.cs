using System.Collections.Generic;
using Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace GuestControlService
{
    public class VisitService: IVisitService
    {
        
        private DbContext _context;
        public VisitService(DbContext context)
        {
            _context = context;
        }
        public Visit Add(Visit visit)
        {
            var res = _context.Set<Visit>().Add(visit);
            _context.SaveChanges();
            return res.Entity;
        }

        public List<Visit> GetAll()
        {
            return _context
                .Set<Visit>()
                .ToList();
        }

        public Visit Get(int Id)
        {
            return _context
                .Set<Visit>()
                .FirstOrDefault(v => v.Id == Id );
        }
    }
    
    public interface IVisitService
    {
        Visit Add(Visit visit);
        List<Visit> GetAll();
        Visit Get(int Id);

    }
}