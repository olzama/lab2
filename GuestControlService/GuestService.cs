using System.Collections.Generic;
using System.Linq;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace GuestControlService
{
    public class GuestService: IGuestService
    {
        
        private DbContext _context;
        public GuestService(DbContext context)
        {
            _context = context;
        }
        public Guest Add(Guest guest)
        {
            var res = _context.Set<Guest>().Add(guest);
            _context.SaveChanges();
            return res.Entity;
        }

        public List<Guest> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public Guest Get(int Id)
        {
            return _context
                .Set<Guest>()
                .FirstOrDefault(g => g.Id == Id );
        }
    }
    
    public interface IGuestService
    {
        Guest Add(Guest guest);
        List<Guest> GetAll();
        Guest Get(int Id);
    }
}