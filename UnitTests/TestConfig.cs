using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Conventions.Internal;
using System;
using Domain;
using Microsoft.Extensions.Configuration;
using System.IO;
using DAL;
using GuestControlService;
using Moq;

namespace UnitTests
{
    public class TestConfig
    {

        public static DbContextOptions<ApplicationDbContext> _options = CreateOptions();
        public static ApplicationDbContext _context = new ApplicationDbContext(_options);
        public GuestService guestService = new GuestService(_context);
        public VisitService visitService = new VisitService(_context);
        
        public static DbContextOptions<ApplicationDbContext> CreateOptions()
        {
            var connectionStringBuilder = new SqliteConnectionStringBuilder
                { DataSource = ":memory:" };
            var connectionString = connectionStringBuilder.ToString();
            var connection = new SqliteConnection(connectionString);
            connection.Open();
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            builder.UseSqlite(connection);
            return builder.Options;
        }

        public Guest seedGuest()
        {
            DatabaseSetup();
            var guest = new Guest()
            {
                FirstName = "TestFirstName",
                LastName = "TestLastName",
                Email = "test@gmail.com"
            };
            return guestService.Add(guest);
        }
        public Visit seedVisit()
        {
            var guest = seedGuest();
            var visit = new Visit()
            {
                Company = "TestCompany",
                TypeOfVisit = "testVisit",
                Planned = DateTime.Now,
                Guest = guest,
                GuestId = guest.Id
            };
            return visitService.Add(visit);
        }
        public void DatabaseSetup()
        {
            _context.Database.EnsureCreated();
        }
    }
}