using NUnit.Framework;

namespace UnitTests
{
    //Requirement 11
    public class ViewVisitsTests
    {
        private static TestConfig _config;
        [SetUp]
        public void Setup()
        {
            _config = new TestConfig();
        }
        [Test]
        public void GetAll_VisitService_ReturnsAllVisits()
        {
            _config.DatabaseSetup();
            var visit = _config.seedVisit();
            var visits = _config.visitService.GetAll();
            Assert.IsNotEmpty(visits);
            Assert.IsTrue(visits.Contains(visit));
        }
    }
}