using System;
using Domain;
using GuestControlService;
using KellermanSoftware.CompareNetObjects;
using NUnit.Framework;

namespace UnitTests
{
    //Requirement 3 
    public class BookVisitTests
    {
        private static TestConfig _config;
        [SetUp]
        public void Setup()
        {
            _config = new TestConfig();
        }
        [Test]
        public void AddVisit_VisitService_AddsVisitAndReturns()
        {
            _config.DatabaseSetup();
            CompareLogic compareLogic = new CompareLogic();
            compareLogic.Config.IgnoreProperty<Visit>(v => v.Id);
            var guest = _config.seedGuest();
            var visit = new Visit()
            {
                Company = "TestCompany",
                TypeOfVisit = "testVisit",
                Planned = DateTime.Now,
                Guest = guest,
                GuestId = guest.Id
            };
            var result = _config.visitService.Add(visit);
            var created = _config.visitService.Get(result.Id);
            Assert.IsNotNull(created);
            Assert.IsTrue(compareLogic.Compare(visit, created).AreEqual);
        }
    }
}