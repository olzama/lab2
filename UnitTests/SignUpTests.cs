using Domain;
using GuestControlService;
using KellermanSoftware.CompareNetObjects;
using NUnit.Framework;

namespace UnitTests
{
    //Requirement 2
    public class SignUpTests
    {
        private static TestConfig _config;
        [SetUp]
        public void Setup()
        {
            _config = new TestConfig();
        }
        [Test]
        public void AddGuest_GuestService_AddsGuestAndReturns()
        {
            _config.DatabaseSetup();
            CompareLogic compareLogic = new CompareLogic();
            compareLogic.Config.IgnoreProperty<Guest>(g => g.Id);
            var guest = new Guest()
            {
                FirstName = "TestFirstName",
                LastName = "TestLastName",
                Email = "test@gmail.com",
                Type = "Employee"
            };
            var result = _config.guestService.Add(guest);
            var created = _config.guestService?.Get(result.Id);
            Assert.IsNotNull(created);
            Assert.IsTrue(compareLogic.Compare(guest, created).AreEqual);
        }
    }
}