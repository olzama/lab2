﻿using System;
using Domain;
using Microsoft.EntityFrameworkCore;
namespace DAL
{
    public class ApplicationDbContext: DbContext
    {
        public DbSet<Guest> Guests { get; set; }  = default!;
        public DbSet<Visit> Visits { get; set; }  = default!;
        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Guest>()
                .Property(p => p.Id)
                .HasDefaultValueSql("NewId()");
            modelBuilder.Entity<Visit>()
                .Property(p => p.Id)
                .HasDefaultValueSql("NewId()");
        }

    }
}